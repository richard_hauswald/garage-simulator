# garage simulator
The awesome garage simulator with the most user friendly UI...

# Getting started
1. clone this project
2. build the maven project using `mvn clean package`
3. run the application using `java -jar target/garage-simulator-jar-with-dependencies.jar -h` to get the help screen

# Confessions
- Half of the tests were written after the implementation.
- The hint to implement this using some sort of established simulation technique was brought to me by a friend of mine.
- The simulation algorithm, core implementation and background information was taken from the pdf
  "Discrete Event Simulation in Java" hosted on http://www.akira.ruc.dk/~keld/research/JAVASIMULATION/ .
  This pdf can also be found in the projects root directory.
