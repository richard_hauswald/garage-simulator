package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.ParkingSpace;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ParkingSpaceFactoryTest {
	@Test
	public void createsTheCorrectNumberOfParkingSpaces() throws Exception {
		final List<ParkingSpace> parkingSpaces = ParkingSpaceFactory.createParkingSpaces(3, 100);
		assertThat(parkingSpaces, is(Matchers.<ParkingSpace>iterableWithSize(300)));
	}

	@Test
	public void createsListContainingParkingSpaces() throws Exception {
		final List<ParkingSpace> parkingSpaces = ParkingSpaceFactory.createParkingSpaces(1, 1);
		final ParkingSpace parkingSpace = parkingSpaces.get(0);
		assertThat(parkingSpace, is(notNullValue()));
	}

	@Test
	public void assignsTheLevelNumber() throws Exception {
		final List<ParkingSpace> parkingSpaces = ParkingSpaceFactory.createParkingSpaces(3, 1);
		assertThat(parkingSpaces.get(0).getLevel(), is(0));
		assertThat(parkingSpaces.get(1).getLevel(), is(1));
		assertThat(parkingSpaces.get(2).getLevel(), is(2));
	}

	@Test
	public void assignsTheSpaceNumber() throws Exception {
		final List<ParkingSpace> parkingSpaces = ParkingSpaceFactory.createParkingSpaces(1, 3);
		assertThat(parkingSpaces.get(0).getParkingSpaceNumber(), is(0));
		assertThat(parkingSpaces.get(1).getParkingSpaceNumber(), is(1));
		assertThat(parkingSpaces.get(2).getParkingSpaceNumber(), is(2));
	}

	@Test
	public void returnsEmptyListForZeroLevels() throws Exception {
		final List<ParkingSpace> parkingSpaces = ParkingSpaceFactory.createParkingSpaces(0, 100);
		assertThat(parkingSpaces, is(notNullValue()));
	}
	@Test
	public void returnsEmptyListForZeroParkingSpacesPerLevel() throws Exception {
		final List<ParkingSpace> parkingSpaces = ParkingSpaceFactory.createParkingSpaces(1, 0);
		assertThat(parkingSpaces, is(notNullValue()));
	}

}
