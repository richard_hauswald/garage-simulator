package de.rhauswald.garagesimulator.simulation;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class SimulatorSanityTest {
	private SimulationSettings.Builder simulationSettingsBuilder;

	@Before
	public void setUp() throws Exception {
		simulationSettingsBuilder = SimulationSettings.simulationSettings()
													  .withSampleProvider(new NotDistributedAtAllSampleProvider())
													  .withSimulationPeriodInSeconds(TimeUnit.HOURS.toSeconds(10))
													  .withAverageCarArrivalTimeInSeconds(30)
													  .withAverageParkingTimeInSeconds(TimeUnit.HOURS.toSeconds(2))
													  .withParkingLevels(2)
													  .withParkingSpacesPerParkingLevel(80);
	}

	@Test
	public void oneSpaceShouldBeEnoughIfCarsAreUnparkingBeforeANewCarArrives() {
		final SimulationSettings simulationSettings = simulationSettingsBuilder
				.withAverageCarArrivalTimeInSeconds(120)
				.withAverageParkingTimeInSeconds(60)
				.build();

		final SimulationRun simulationRun = Simulator.run(simulationSettings);

		final int maxParkingSpacesInUse = simulationRun.getMaxParkingSpacesInUse();
		assertThat(maxParkingSpacesInUse, is(1));
	}

	@Test
	public void theAmountOfParkingSpacesMustBeBigEnoughToBufferTheParkingTime() {
		final SimulationSettings simulationSettings = simulationSettingsBuilder
				.withAverageCarArrivalTimeInSeconds(TimeUnit.MINUTES.toSeconds(1))
				.withAverageParkingTimeInSeconds(TimeUnit.HOURS.toSeconds(1))
				.withParkingLevels(1)
				.withParkingSpacesPerParkingLevel(60)
				.build();

		final SimulationRun simulationRun = Simulator.run(simulationSettings);

		final int maxParkingSpacesInUse = simulationRun.getMaxParkingSpacesInUse();
		assertThat(maxParkingSpacesInUse, is(60));
		final int vehiclesRejected = simulationRun.getVehiclesRejected();
		assertThat(vehiclesRejected, is(0));
	}

	@Test
	public void theNumberOfCreatedVehiclesShouldBeCorrect() {
		final SimulationSettings simulationSettings = simulationSettingsBuilder
				.withAverageCarArrivalTimeInSeconds(10)
				.withSimulationPeriodInSeconds(100)
				.withAverageParkingTimeInSeconds(1)
				.build();

		final SimulationRun simulationRun = Simulator.run(simulationSettings);

		assertThat(simulationRun.getVehiclesFoundAParkingSpace(), is(10));
	}

	@Test
	public void theNumberOfUnparkedVehiclesShouldBeCorrect() {
		final SimulationSettings simulationSettings = simulationSettingsBuilder
				.withAverageCarArrivalTimeInSeconds(10)
				.withSimulationPeriodInSeconds(100)
				.withAverageParkingTimeInSeconds(1)
				.build();

		final SimulationRun simulationRun = Simulator.run(simulationSettings);

		assertThat(simulationRun.getVehiclesUnparked(), is(9));
	}

	@Test
	public void theNumberOfRejectedVehiclesShouldBeCorrect() {
		final SimulationSettings simulationSettings = simulationSettingsBuilder
				.withAverageCarArrivalTimeInSeconds(10)
				.withSimulationPeriodInSeconds(100)
				.withParkingLevels(0)
				.build();

		final SimulationRun simulationRun = Simulator.run(simulationSettings);

		assertThat(simulationRun.getVehiclesRejected(), is(10));
	}

}
