package de.rhauswald.garagesimulator.simulation;

public class NotDistributedAtAllSampleProvider implements SampleProvider {
	@Override
	public double sampleForMean(double mean) {
		return mean;
	}
}
