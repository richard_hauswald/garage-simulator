package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.Vehicle;
import de.rhauswald.garagesimulator.entities.vehicles.Car;
import de.rhauswald.garagesimulator.entities.vehicles.MotorBike;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RandomVehicleFactoryTest {
	private Map<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability;
	private Map<Class<? extends Vehicle>, Integer> distribution;

	@Before
	public void setUp() throws Exception {
		vehicleTypesAndOccurrenceProbability = new HashMap<>();
		distribution = new HashMap<>();
		distribution.put(Car.class, 0);
		distribution.put(MotorBike.class, 0);
	}

	@Test
	public void testNewVehicleDistribution() throws Exception {
		vehicleTypesAndOccurrenceProbability.put(Car.class, 70);
		vehicleTypesAndOccurrenceProbability.put(MotorBike.class, 30);
		final RandomVehicleFactory sut = new RandomVehicleFactory(vehicleTypesAndOccurrenceProbability);

		for (int i = 0; i < 1000; i++) {
			final Vehicle vehicle = sut.newVehicle();
			refreshDistribution(vehicle);
		}

		final Integer carDistribution = distribution.get(Car.class);
		assertThat(carDistribution, allOf(greaterThan(650), lessThan(750)));

		final Integer motorBikeDistribution = distribution.get(MotorBike.class);
		assertThat(motorBikeDistribution, allOf(greaterThan(250), lessThan(350)));
	}

	private void refreshDistribution(Vehicle vehicle) {
		final Class<? extends Vehicle> vehicleClass = vehicle.getClass();
		final int increment = distribution.get(vehicleClass) + 1;
		distribution.put(vehicleClass, increment);
	}
}
