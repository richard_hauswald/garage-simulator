package de.rhauswald.garagesimulator.simulation;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ExponentialSampleProviderTest {
	private ExponentialSampleProvider exponentialSampleProvider;

	@Before
	public void setUp() throws Exception {
		exponentialSampleProvider = new ExponentialSampleProvider();
	}

	@Test
	public void generatesSamples() throws Exception {
		final Double sampleForMean = exponentialSampleProvider.sampleForMean(300.0);
		assertThat(sampleForMean, greaterThan(0.0));
	}

	@Test
	public void generatesSamplesInARandomWaySoThatThereAreDifferentSamplesInAtLeast80PercentOfTheCases() throws Exception {
		final int sampleSize = 10;
		Set<Double> samplesTaken = new HashSet<>(sampleSize);
		for (int i = 0; i < sampleSize; i++) {
			samplesTaken.add(exponentialSampleProvider.sampleForMean(300.0));
		}
		assertThat(samplesTaken.size(), greaterThan(8));
	}
}
