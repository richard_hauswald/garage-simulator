package de.rhauswald.garagesimulator.cli;

import de.rhauswald.garagesimulator.entities.Vehicle;
import de.rhauswald.garagesimulator.entities.vehicles.Car;
import de.rhauswald.garagesimulator.entities.vehicles.MotorBike;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class VehicleTypeScannerTest {
	@Test
	public void testScanVehicleTypes() throws Exception {
		final List<String> vehicleTypes = VehicleTypeScanner.scanVehicleTypes();
		assertThat(vehicleTypes, containsInAnyOrder("Car", "MotorBike"));
	}

	@Test
	public void returnsCarClassForVehicleTypeCar() throws Exception {
		final Class<? extends Vehicle> carClass = Car.class;
		final Class<? extends Vehicle> classForType = VehicleTypeScanner.classForType("Car");
		assertThat(classForType, Matchers.<Class<? extends Vehicle>>is(carClass));
	}

	@Test
	public void returnsMotorBikeClassForVehicleTypeMotorBike() throws Exception {
		final Class<? extends Vehicle> motorBikeClass = MotorBike.class;
		final Class<? extends Vehicle> classForType = VehicleTypeScanner.classForType("MotorBike");
		assertThat(classForType, Matchers.<Class<? extends Vehicle>>is(motorBikeClass));
	}
}
