package de.rhauswald.garagesimulator.cli;

import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

public class VehicleTypeDefaultWeightCalculatorTest {

	@Test
	public void returns50ForTwoVehicleTypes() throws Exception {
		final int defaultWeight = VehicleTypeDefaultWeightCalculator.defaultWeight(2);
		assertThat(defaultWeight, is(50));
	}

	@Test
	public void roundsSoItsReturning33ForThreeVehicleTypes() throws Exception {
		final int defaultWeight = VehicleTypeDefaultWeightCalculator.defaultWeight(3);
		assertThat(defaultWeight, is(33));
	}


	@Test
	public void roundsSoItsReturning16ForThreeVehicleTypes() throws Exception {
		final int defaultWeight = VehicleTypeDefaultWeightCalculator.defaultWeight(6);
		assertThat(defaultWeight, is(16));
	}

}
