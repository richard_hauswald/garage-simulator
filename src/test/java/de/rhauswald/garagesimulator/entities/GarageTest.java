package de.rhauswald.garagesimulator.entities;

import de.rhauswald.garagesimulator.entities.vehicles.Car;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GarageTest {
	@Rule
	public final ExpectedException expectedException = ExpectedException.none();
	private List<ParkingSpace> freeParkingSpaces;
	private Garage garage;

	@Before
	public void setUp() throws Exception {
		freeParkingSpaces = new ArrayList<>();
		freeParkingSpaces.add(new ParkingSpace(0, 0));
		Map<String, ParkingSpace> usedParkingSpaces = new HashMap<>();
		garage = new Garage(freeParkingSpaces, usedParkingSpaces);
	}

	@Test
	public void vehiclesCanBeParkedIfThereAreFreeParkingSpaces() throws Exception {
		final Vehicle vehicle = aVehicle();

		garage.park(vehicle);
	}

	@Test
	public void freeParkingSpacesIsDecrementedWhenAVehicleIsParked() throws Exception {
		final Vehicle vehicle = aVehicle();
		final int freeParkingSpacesBeforePark = garage.freeParkingSpaces();

		garage.park(vehicle);

		assertThat(garage.freeParkingSpaces(), is(equalTo(freeParkingSpacesBeforePark - 1)));
	}

	@Test
	public void usedParkingSpacesIsIncrementedWhenAVehicleIsParked() throws Exception {
		final Vehicle vehicle = aVehicle();
		final int usedParkingSpacesBeforePark = garage.usedParkingSpaces();

		garage.park(vehicle);

		assertThat(garage.usedParkingSpaces(), is(equalTo(usedParkingSpacesBeforePark + 1)));
	}

	@Test
	public void vehiclesAreRejectedIfThereAreNoFreeParkingSpaces() throws Exception {
		final Vehicle vehicle = aVehicle();
		freeParkingSpaces.clear();

		expectedException.expect(NoFreeParkingLotsException.class);

		garage.park(vehicle);
	}

	@Test
	public void vehicleCanBeUnparked() throws Exception {
		final Vehicle vehicle = aVehicle();
		garage.park(vehicle);

		garage.unpark(vehicle);
	}

	@Test
	public void unparkingAVehicleDoesNotRequiredTheExactSameInstanceOfTheVehicle() throws Exception {
		final Vehicle vehicle = aVehicle();
		garage.park(vehicle);

		final Vehicle newInstanceOfTheSameVehicle = aVehicle();
		garage.unpark(newInstanceOfTheSameVehicle);
	}

	@Test
	public void freeParkingSpacesIsIncrementedWhenAVehicleIsUnparked() throws Exception {
		final Vehicle vehicle = aVehicle();
		garage.park(vehicle);

		final int freeParkingSpacesBeforeUnpark = garage.freeParkingSpaces();
		garage.unpark(vehicle);

		assertThat(garage.freeParkingSpaces(), is(equalTo(freeParkingSpacesBeforeUnpark + 1)));
	}

	@Test
	public void usedParkingSpacesIsDecrementedWhenAVehicleIsUnparked() throws Exception {
		final Vehicle vehicle = aVehicle();
		garage.park(vehicle);

		final int usedParkingSpacesBeforeUnpark = garage.usedParkingSpaces();
		garage.unpark(vehicle);

		assertThat(garage.usedParkingSpaces(), is(equalTo(usedParkingSpacesBeforeUnpark - 1)));
	}

	@Test
	public void aGarageWithOneParkingSpaceCanParkAndUnparkAVehicleMultipleTimes() throws Exception {
		final Vehicle vehicle = aVehicle();

		for (int i = 0; i < 3; i++) {
			garage.park(vehicle);
			garage.unpark(vehicle);
		}
	}

	@Test
	public void anAttemptToUnparkACarNotParkedInTheGarageWillCauseAUnsupportedOperationExceptionContainingTheVehiclesLicensePlate() throws Exception {
		final Vehicle vehicle = aVehicle();

		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage(containsString(vehicle.getLicensePlate()));
		garage.unpark(vehicle);
	}


	@Test
	public void theParkingSpaceOfAParkedVehicleIsRetrievable() throws Exception {
		final Vehicle vehicle = aVehicle();
		garage.park(vehicle);

		final ParkingSpace parkingSpace = garage.parkingSpaceOfVehicle(vehicle.getLicensePlate());
		assertThat(parkingSpace, is(notNullValue()));
	}


	@Test
	public void nullWillBeReturnedAsParkingSpaceOfAVehicleNotParkedInTheGarage() throws Exception {
		final Vehicle vehicle = aVehicle();

		final ParkingSpace parkingSpace = garage.parkingSpaceOfVehicle(vehicle.getLicensePlate());
		assertThat(parkingSpace, is(nullValue()));
	}

	@Test
	public void theParkingSpaceOfAParkedVehicleContainsTheLevelAndTheParkingSpaceNumber() throws Exception {
		final Vehicle vehicle = aVehicle();
		freeParkingSpaces.clear();
		freeParkingSpaces.add(new ParkingSpace(23, 1337));
		garage.park(vehicle);

		final ParkingSpace parkingSpace = garage.parkingSpaceOfVehicle(vehicle.getLicensePlate());
		assertThat(parkingSpace.getLevel(), is(23));
		assertThat(parkingSpace.getParkingSpaceNumber(), is(1337));
	}

	@Test
	public void returnsTheCorrectNumberOfOverallParkingSpacesWhenGarageIsEmpty() throws Exception {
		freeParkingSpaces.clear();
		freeParkingSpaces.add(new ParkingSpace(0, 0));
		freeParkingSpaces.add(new ParkingSpace(0, 1));
		freeParkingSpaces.add(new ParkingSpace(0, 2));

		assertThat(garage.getOverallNumberOfParkingSpaces(), is(3));
	}

	@Test
	public void returnsTheCorrectNumberOfOverallParkingSpacesWhenGarageFull() throws Exception {
		freeParkingSpaces.clear();
		freeParkingSpaces.add(new ParkingSpace(0, 0));
		freeParkingSpaces.add(new ParkingSpace(0, 1));
		freeParkingSpaces.add(new ParkingSpace(0, 2));

		garage.park(aVehicleWithRandomNumberPlate());
		garage.park(aVehicleWithRandomNumberPlate());
		garage.park(aVehicleWithRandomNumberPlate());

		assertThat(garage.getOverallNumberOfParkingSpaces(), is(3));
	}

	@Test
	public void returnsTheCorrectNumberOfOverallParkingSpacesWhenGaragePartiallyUsed() throws Exception {
		freeParkingSpaces.clear();
		freeParkingSpaces.add(new ParkingSpace(0, 0));
		freeParkingSpaces.add(new ParkingSpace(0, 1));
		freeParkingSpaces.add(new ParkingSpace(0, 2));

		garage.park(aVehicleWithRandomNumberPlate());
		garage.park(aVehicleWithRandomNumberPlate());

		assertThat(garage.getOverallNumberOfParkingSpaces(), is(3));
	}

	private Vehicle aVehicleWithRandomNumberPlate() {
		return new Car(UUID.randomUUID().toString());
	}
	private Vehicle aVehicle() {
		return new Car("M - VL 1337");
	}
}
