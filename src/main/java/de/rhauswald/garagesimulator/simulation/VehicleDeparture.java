package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.Garage;
import de.rhauswald.garagesimulator.entities.Vehicle;
import de.rhauswald.javasimulation.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VehicleDeparture extends Event {
	private static final Logger LOGGER = LoggerFactory.getLogger(VehicleDeparture.class);
	private final SimulationSettings simulationSettings;
	private final SimulationRun simulationRun;
	private final Garage garage;
	private final Vehicle vehicle;

	public VehicleDeparture(SimulationSettings simulationSettings, SimulationRun simulationRun, Garage garage, Vehicle vehicle) {
		this.simulationSettings = simulationSettings;
		this.simulationRun = simulationRun;
		this.garage = garage;
		this.vehicle = vehicle;
	}

	@Override
	protected void actions() {
		garage.unpark(vehicle);
		simulationRun.oneMoreVehicleUnparked();
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("simulated time: {} - vehicle unparked", time());
		}
	}

	public void schedule() {
		SampleProvider sampleProvider = simulationSettings.getSampleProvider();
		double averageParkingTimeInSeconds = simulationSettings.getAverageParkingTimeInSeconds();
		final double parkingTimeInSeconds = sampleProvider.sampleForMean(averageParkingTimeInSeconds);
		final double evTime = time() + parkingTimeInSeconds;
		schedule(evTime);
	}
}
