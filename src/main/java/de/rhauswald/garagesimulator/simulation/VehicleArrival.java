package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.Garage;
import de.rhauswald.garagesimulator.entities.NoFreeParkingLotsException;
import de.rhauswald.garagesimulator.entities.Vehicle;
import de.rhauswald.javasimulation.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VehicleArrival extends Event {
	private static final Logger LOGGER = LoggerFactory.getLogger(VehicleArrival.class);

	private final SimulationSettings simulationSettings;
	private final RandomVehicleFactory randomVehicleFactory;
	private final SimulationRun simulationRun;
	private final Garage garage;

	public VehicleArrival(SimulationSettings simulationSettings, SimulationRun simulationRun,
						  RandomVehicleFactory randomVehicleFactory, Garage garage) {
		this.simulationSettings = simulationSettings;
		this.simulationRun = simulationRun;
		this.randomVehicleFactory = randomVehicleFactory;
		this.garage = garage;
	}

	@Override
	protected void actions() {
		final Vehicle vehicle = randomVehicleFactory.newVehicle();

		try {
			garage.park(vehicle);
			logDebug("vehicle parked");
			simulationRun.oneMoreVehicleParked();
			final VehicleDeparture nextDeparture = newDepartureEvent(vehicle);
			nextDeparture.schedule();
		} catch (NoFreeParkingLotsException e) {
			logDebug("vehicle rejected");
			simulationRun.oneMoreVehicleRejected();
		}

		final VehicleArrival nextArrival = newArrivalEvent();
		nextArrival.schedule();
	}

	void schedule() {
		final SampleProvider sampleProvider = simulationSettings.getSampleProvider();
		final double averageCarArrivalTimeInSeconds = simulationSettings.getAverageCarArrivalTimeInSeconds();
		final double nextCarArrival = sampleProvider.sampleForMean(averageCarArrivalTimeInSeconds);
		double evTime = time() + nextCarArrival;
		schedule(evTime);
	}

	private VehicleDeparture newDepartureEvent(Vehicle vehicle) {
		return new VehicleDeparture(simulationSettings, simulationRun, garage, vehicle);
	}

	private VehicleArrival newArrivalEvent() {
		return new VehicleArrival(simulationSettings, simulationRun, randomVehicleFactory, garage);
	}

	private void logDebug(String whatHappened) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("simulated time: {} - {}", time(), whatHappened);
		}
	}
}
