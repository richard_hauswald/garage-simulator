package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.Vehicle;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

final class RandomVehicleFactory {
	private final Random random;
	private final Map<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability;
	private final int chanceTotal;

	RandomVehicleFactory(Map<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability) {
		this.vehicleTypesAndOccurrenceProbability = new HashMap<>(vehicleTypesAndOccurrenceProbability);
		random = new Random();

		int chanceTotalCounter = 0;
		for (Integer probability : vehicleTypesAndOccurrenceProbability.values()) {
			chanceTotalCounter += probability;
		}
		this.chanceTotal = chanceTotalCounter;
	}

	Vehicle newVehicle() {

		final Class<? extends Vehicle> vehicleClass = selectVehicleClass();
		final Vehicle vehicle = newInstance(vehicleClass);
		return vehicle;
	}

	/**
	 * Shamelessly copied from from http://forums.bukkit.org/threads/help-with-fake-random.85467/
	 */
	private Class<? extends Vehicle> selectVehicleClass() {
		int choice = random.nextInt(chanceTotal), subtotal = 0;
		for (Map.Entry<Class<? extends Vehicle>, Integer> entry : vehicleTypesAndOccurrenceProbability.entrySet()) {
			subtotal += entry.getValue();
			if (choice < subtotal) {
				final Class<? extends Vehicle> vehicleClass = entry.getKey();
				return vehicleClass;
			}
		}
		throw new RuntimeException("This should never happen :)");
	}

	private Vehicle newInstance(Class<? extends Vehicle> vehicleClass) {
		try {
			final Constructor<? extends Vehicle> vehicleConstructor = vehicleClass.getDeclaredConstructor(String.class);
			return vehicleConstructor.newInstance(UUID.randomUUID().toString());
		} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
			throw new RuntimeException("Could not create vehicle instance for vehicle class " + vehicleClass, e);
		}
	}
}
