package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.Garage;

import java.util.concurrent.TimeUnit;

public class SimulationRun {
	private final Garage garage;
	private int vehiclesFoundAParkingSpace;
	private int vehiclesRejected;
	private int vehiclesUnparked;
	private int maxParkingSpacesInUse;

	public SimulationRun(Garage garage) {
		this.garage = garage;
		vehiclesFoundAParkingSpace = 0;
		vehiclesRejected = 0;
		vehiclesUnparked = 0;
		maxParkingSpacesInUse = 0;
	}

	public void oneMoreVehicleParked() {
		this.vehiclesFoundAParkingSpace++;

		final int currentlyUsedParkingSpaces = garage.usedParkingSpaces();
		if (maxParkingSpacesInUse < currentlyUsedParkingSpaces) {
			this.maxParkingSpacesInUse = currentlyUsedParkingSpaces;
		}
	}

	public int getOverallNumberOfParkingSpaces() {
		return garage.getOverallNumberOfParkingSpaces();
	}

	public void oneMoreVehicleRejected() {
		this.vehiclesRejected++;
	}

	public void oneMoreVehicleUnparked() {
		this.vehiclesUnparked++;
	}

	public int getVehiclesFoundAParkingSpace() {
		return vehiclesFoundAParkingSpace;
	}

	public int getVehiclesRejected() {
		return vehiclesRejected;
	}

	public int getVehiclesUnparked() {
		return vehiclesUnparked;
	}

	public int getMaxParkingSpacesInUse() {
		return maxParkingSpacesInUse;
	}

	@Override
	public String toString() {
		return "SimulationRun{" +
				super.toString() +
				", vehiclesFoundAParkingSpace=" + vehiclesFoundAParkingSpace +
				", vehiclesRejected=" + vehiclesRejected +
				", vehiclesUnparked=" + vehiclesUnparked +
				", maxParkingSpacesInUse=" + maxParkingSpacesInUse +
				'}';
	}
}
