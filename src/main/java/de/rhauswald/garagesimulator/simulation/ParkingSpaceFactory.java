package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.ParkingSpace;

import java.util.ArrayList;
import java.util.List;

class ParkingSpaceFactory {
	static List<ParkingSpace> createParkingSpaces(int levels, int parkingSpacesPerLevel) {
		final int capacity = levels * parkingSpacesPerLevel;
		List<ParkingSpace> parkingSpaces = new ArrayList<>(capacity);
		for (int currentLevel = 0; currentLevel < levels; currentLevel++) {
			for (int currentParkingSpaceNumber = 0; currentParkingSpaceNumber < parkingSpacesPerLevel; currentParkingSpaceNumber++) {
				final ParkingSpace parkingSpace = new ParkingSpace(currentLevel, currentParkingSpaceNumber);
				parkingSpaces.add(parkingSpace);
			}
		}
		return parkingSpaces;
	}
}
