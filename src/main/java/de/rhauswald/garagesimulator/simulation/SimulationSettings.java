package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.Vehicle;
import de.rhauswald.garagesimulator.entities.vehicles.Car;
import de.rhauswald.garagesimulator.entities.vehicles.MotorBike;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class SimulationSettings {
	private final double simulationPeriodInSeconds;
	private final double averageCarArrivalTimeInSeconds;
	private final double averageParkingTimeInSeconds;
	private final Map<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability;
	private final int parkingLevels;
	private final int parkingSpacesPerParkingLevel;
	private final SampleProvider sampleProvider;

	private SimulationSettings(double simulationPeriodInSeconds, double averageCarArrivalTimeInSeconds, double averageParkingTimeInSeconds, Map<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability, int parkingLevels, int parkingSpacesPerParkingLevel, SampleProvider sampleProvider) {
		this.simulationPeriodInSeconds = simulationPeriodInSeconds;
		this.averageCarArrivalTimeInSeconds = averageCarArrivalTimeInSeconds;
		this.averageParkingTimeInSeconds = averageParkingTimeInSeconds;
		this.vehicleTypesAndOccurrenceProbability = vehicleTypesAndOccurrenceProbability;
		this.parkingLevels = parkingLevels;
		this.parkingSpacesPerParkingLevel = parkingSpacesPerParkingLevel;
		this.sampleProvider = sampleProvider;
	}

	public double getAverageCarArrivalTimeInSeconds() {
		return averageCarArrivalTimeInSeconds;
	}

	public double getAverageParkingTimeInSeconds() {
		return averageParkingTimeInSeconds;
	}

	public double getSimulationPeriodInSeconds() {
		return simulationPeriodInSeconds;
	}

	public Map<Class<? extends Vehicle>, Integer> getVehicleTypesAndOccurrenceProbability() {
		return vehicleTypesAndOccurrenceProbability;
	}

	public int getParkingLevels() {
		return parkingLevels;
	}

	public int getParkingSpacesPerParkingLevel() {
		return parkingSpacesPerParkingLevel;
	}

	public static Builder simulationSettings() {
		return new Builder();
	}

	public SampleProvider getSampleProvider() {
		return sampleProvider;
	}

	public static class Builder {
		private double simulationPeriodInSeconds = TimeUnit.HOURS.toSeconds(12);
		private double averageCarArrivalTimeInSeconds = 30;
		private double averageParkingTimeInSeconds = TimeUnit.HOURS.toSeconds(1);
		private Map<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability;
		private int parkingLevels = 2;
		private int parkingSpacesPerParkingLevel = 80;
		private SampleProvider sampleProvider = new ExponentialSampleProvider();

		public Builder() {
			this.vehicleTypesAndOccurrenceProbability = new HashMap<>();
			this.vehicleTypesAndOccurrenceProbability.put(Car.class, 70);
			this.vehicleTypesAndOccurrenceProbability.put(MotorBike.class, 30);
		}

		public Builder withSimulationPeriodInSeconds(double simulationPeriodInSeconds) {
			this.simulationPeriodInSeconds = simulationPeriodInSeconds;
			return this;
		}

		public Builder withAverageCarArrivalTimeInSeconds(double averageCarArrivalTimeInSeconds) {
			this.averageCarArrivalTimeInSeconds = averageCarArrivalTimeInSeconds;
			return this;
		}

		public Builder withAverageParkingTimeInSeconds(double averageParkingTimeInSeconds) {
			this.averageParkingTimeInSeconds = averageParkingTimeInSeconds;
			return this;
		}

		public Builder withVehicleTypesAndOccurrenceProbability(Map<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability) {
			this.vehicleTypesAndOccurrenceProbability = vehicleTypesAndOccurrenceProbability;
			return this;
		}

		public Builder withParkingLevels(int parkingLevels) {
			this.parkingLevels = parkingLevels;
			return this;
		}

		public Builder withParkingSpacesPerParkingLevel(int parkingSpacesPerParkingLevel) {
			this.parkingSpacesPerParkingLevel = parkingSpacesPerParkingLevel;
			return this;
		}

		public Builder withSampleProvider(SampleProvider sampleProvider) {
			this.sampleProvider = sampleProvider;
			return this;
		}

		public SimulationSettings build() {
			SimulationSettings simulationSettings = new SimulationSettings(simulationPeriodInSeconds, averageCarArrivalTimeInSeconds, averageParkingTimeInSeconds, vehicleTypesAndOccurrenceProbability, parkingLevels, parkingSpacesPerParkingLevel, sampleProvider);
			return simulationSettings;
		}
	}
}
