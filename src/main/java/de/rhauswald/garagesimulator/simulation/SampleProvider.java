package de.rhauswald.garagesimulator.simulation;

public interface SampleProvider {
	double sampleForMean(double mean);
}
