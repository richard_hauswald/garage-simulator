package de.rhauswald.garagesimulator.simulation;

import org.apache.commons.math3.distribution.ExponentialDistribution;

public class ExponentialSampleProvider implements SampleProvider {
	@Override
	public double sampleForMean(double mean) {
		final ExponentialDistribution exponentialDistribution = new ExponentialDistribution(mean);
		final double sample = exponentialDistribution.sample();
		return sample;
	}
}
