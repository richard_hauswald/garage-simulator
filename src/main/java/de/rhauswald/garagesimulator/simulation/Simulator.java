package de.rhauswald.garagesimulator.simulation;

import de.rhauswald.garagesimulator.entities.Garage;
import de.rhauswald.garagesimulator.entities.ParkingSpace;
import de.rhauswald.garagesimulator.entities.Vehicle;
import de.rhauswald.javasimulation.Simulation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Simulator extends Simulation {
	public static SimulationRun run(SimulationSettings simulationSettings) {
		final Garage garage = garage(simulationSettings);
		final SimulationRun simulationRun = new SimulationRun(garage);
		final RandomVehicleFactory randomVehicleFactory = randomVehicleFactory(simulationSettings);

		final VehicleArrival vehicleArrival = new VehicleArrival(simulationSettings, simulationRun, randomVehicleFactory, garage);
		vehicleArrival.schedule();

		runSimulation(simulationSettings.getSimulationPeriodInSeconds());

		return simulationRun;
	}

	private static Garage garage(SimulationSettings simulationSettings) {
		final int parkingLevels = simulationSettings.getParkingLevels();
		final int parkingSpacesPerParkingLevel = simulationSettings.getParkingSpacesPerParkingLevel();
		final List<ParkingSpace> parkingSpaces = ParkingSpaceFactory.createParkingSpaces(parkingLevels, parkingSpacesPerParkingLevel);
		final Map<String, ParkingSpace> usedParkingSpaces = new HashMap<>();
		return new Garage(parkingSpaces, usedParkingSpaces);
	}

	private static RandomVehicleFactory randomVehicleFactory(SimulationSettings simulationSettings) {
		final Map<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability = simulationSettings.getVehicleTypesAndOccurrenceProbability();
		return new RandomVehicleFactory(vehicleTypesAndOccurrenceProbability);
	}
}
