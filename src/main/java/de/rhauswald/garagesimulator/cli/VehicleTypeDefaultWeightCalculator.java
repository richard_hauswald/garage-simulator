package de.rhauswald.garagesimulator.cli;

class VehicleTypeDefaultWeightCalculator {
	static int defaultWeight(int numberOfVehicleTypes) {
		return 100 / numberOfVehicleTypes;
	}
}
