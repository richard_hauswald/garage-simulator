package de.rhauswald.garagesimulator.cli;

import de.rhauswald.garagesimulator.entities.Vehicle;
import de.rhauswald.garagesimulator.simulation.SimulationRun;
import de.rhauswald.garagesimulator.simulation.SimulationSettings;
import de.rhauswald.garagesimulator.simulation.Simulator;
import joptsimple.ArgumentAcceptingOptionSpec;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.util.Arrays.asList;

public class Cli {

	public static void main(String[] args) throws IOException {
		OptionParser parser = new OptionParser();
		final ArgumentAcceptingOptionSpec<Integer> simulationPeriodOptionSpec = parser
				.acceptsAll(asList("sp", "simulationPeriod"))
				.withRequiredArg()
				.ofType(Integer.class)
				.describedAs("simulation period in hours")
				.defaultsTo(12);
		final ArgumentAcceptingOptionSpec<Integer> averageCarArrivalTimeInSecondsOptionSpec = parser
				.acceptsAll(asList("cat", "averageCarArrivalTime"))
				.withRequiredArg()
				.ofType(Integer.class)
				.describedAs("average car arrival time in seconds, eg a car arrives every <value> seconds")
				.defaultsTo(30);
		final ArgumentAcceptingOptionSpec<Integer> averageParkingTimeInSecondsOptionSpec = parser
				.acceptsAll(asList("pt", "averageParkingTime"))
				.withRequiredArg()
				.ofType(Integer.class)
				.describedAs("average parking time in seconds")
				.defaultsTo((Long.valueOf(TimeUnit.HOURS.toSeconds(1)).intValue()));
		final ArgumentAcceptingOptionSpec<Integer> parkingLevelsOptionSpec = parser
				.acceptsAll(asList("pl", "parkingLevels"))
				.withRequiredArg()
				.ofType(Integer.class)
				.describedAs("parking levels of the garage")
				.defaultsTo(3);
		final ArgumentAcceptingOptionSpec<Integer> parkingSpacesPerParkingLevelOptionSpec = parser
				.acceptsAll(asList("ps", "parkingSpacesPerParkingLevel"))
				.withRequiredArg()
				.ofType(Integer.class)
				.describedAs("parking spaces per parking level of the garage")
				.defaultsTo(80);

		final Map<String, ArgumentAcceptingOptionSpec<Integer>> vehicleTypeOptionSpecs = vehicleTypeOptionSpecs(parser);
		final OptionSpec<Void> helpOptionSpec = parser.acceptsAll(asList("h", "?"), "show help").forHelp();

		final OptionSet optionSet = parser.parse(args);

		if (optionSet.has(helpOptionSpec)) {
			parser.printHelpOn(System.out);
		} else {
			final Integer simulationPeriod = optionSet.valueOf(simulationPeriodOptionSpec);
			final double simulationPeriodInSeconds = TimeUnit.HOURS.toSeconds(simulationPeriod);
			final Integer parkingLevels = optionSet.valueOf(parkingLevelsOptionSpec);
			final Integer parkingSpacesPerParkingLevel = optionSet.valueOf(parkingSpacesPerParkingLevelOptionSpec);
			final Integer averageCarArrivalTimeInSeconds = optionSet.valueOf(averageCarArrivalTimeInSecondsOptionSpec);
			final Integer averageParkingTimeInSeconds = optionSet.valueOf(averageParkingTimeInSecondsOptionSpec);
			final HashMap<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability = vehicleTypesAndProbability(vehicleTypeOptionSpecs, optionSet);

			final SimulationSettings settings = SimulationSettings
					.simulationSettings()
					.withSimulationPeriodInSeconds(simulationPeriodInSeconds)
					.withParkingLevels(parkingLevels)
					.withParkingSpacesPerParkingLevel(parkingSpacesPerParkingLevel)
					.withAverageCarArrivalTimeInSeconds(averageCarArrivalTimeInSeconds)
					.withAverageParkingTimeInSeconds(averageParkingTimeInSeconds)
					.withVehicleTypesAndOccurrenceProbability(vehicleTypesAndOccurrenceProbability)
					.build();

			final SimulationRun simulationRun = Simulator.run(settings);
			ReportPrinter.printReport(simulationRun);
		}
	}

	private static Map<String, ArgumentAcceptingOptionSpec<Integer>> vehicleTypeOptionSpecs(OptionParser parser) {
		final List<String> vehicleTypes = VehicleTypeScanner.scanVehicleTypes();
		final Map<String, ArgumentAcceptingOptionSpec<Integer>> vehicleTypeOptionSpecs = new HashMap<>();
		int defaultWeight = VehicleTypeDefaultWeightCalculator.defaultWeight(vehicleTypes.size());
		for (String vehicleType : vehicleTypes) {
			final ArgumentAcceptingOptionSpec<Integer> optionSpec = parser
					.accepts(vehicleType + "Weight")
					.withRequiredArg()
					.ofType(Integer.class)
					.describedAs("The weight of the vehicle type " + vehicleType + ", eg --" + vehicleType + "Weight=" + defaultWeight)
					.defaultsTo(defaultWeight);
			vehicleTypeOptionSpecs.put(vehicleType, optionSpec);
		}
		return vehicleTypeOptionSpecs;
	}

	private static HashMap<Class<? extends Vehicle>, Integer> vehicleTypesAndProbability(Map<String, ArgumentAcceptingOptionSpec<Integer>> vehicleTypeOptionSpecs, OptionSet optionSet) {
		final HashMap<Class<? extends Vehicle>, Integer> vehicleTypesAndOccurrenceProbability = new HashMap<>(vehicleTypeOptionSpecs.size());
		for (Map.Entry<String, ArgumentAcceptingOptionSpec<Integer>> optionSpecEntry : vehicleTypeOptionSpecs.entrySet()) {
			final Integer weight = optionSet.valueOf(optionSpecEntry.getValue());
			final String vehicleType = optionSpecEntry.getKey();
			final Class<? extends Vehicle> vehicleTypeClass = VehicleTypeScanner.classForType(vehicleType);
			vehicleTypesAndOccurrenceProbability.put(vehicleTypeClass, weight);
		}
		return vehicleTypesAndOccurrenceProbability;
	}
}
