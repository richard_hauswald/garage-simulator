package de.rhauswald.garagesimulator.cli;

import de.rhauswald.garagesimulator.simulation.SimulationRun;

class ReportPrinter {
	static void printReport(SimulationRun simulationRun) {
		print("vehicles found a parking space", simulationRun.getVehiclesFoundAParkingSpace());
		print("vehicles left the garage", simulationRun.getVehiclesUnparked());
		print("rejected vehicles", simulationRun.getVehiclesRejected());
		print("overall number of parking spaces", simulationRun.getOverallNumberOfParkingSpaces());
		print("max number of parking spaces in use", simulationRun.getMaxParkingSpacesInUse());
	}

	private static void print(String caption, Object value) {
		System.out.println(caption + ": " + String.valueOf(value));
	}
}
