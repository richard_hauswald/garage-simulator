package de.rhauswald.garagesimulator.cli;

import de.rhauswald.garagesimulator.entities.Vehicle;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class VehicleTypeScanner {
	static List<String> scanVehicleTypes() {
		final String aPackageName = basePackageName();
		Reflections reflections = new Reflections(aPackageName);
		Set<Class<? extends Vehicle>> vehicleImplementationClasses = reflections.getSubTypesOf(Vehicle.class);
		final ArrayList<String> vehicleTypes = new ArrayList<>(vehicleImplementationClasses.size());
		for (Class<? extends Vehicle> vehicleImplementationClass : vehicleImplementationClasses) {
			vehicleTypes.add(vehicleImplementationClass.getSimpleName());
		}
		return vehicleTypes;
	}

	@SuppressWarnings("unchecked")
	static Class<? extends Vehicle> classForType(String vehicleType) {
		final String basePackageName = basePackageName();
		final String fqcn = basePackageName + "." + vehicleType;
		try {
			return (Class<? extends Vehicle>) Class.forName(fqcn);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("No vehicle class found for vehicleType " + vehicleType);
		}
	}

	private static String basePackageName() {
		final Package aPackage = Vehicle.class.getPackage();
		return aPackage.getName() + ".vehicles";
	}
}
