package de.rhauswald.garagesimulator.entities;

public abstract class Vehicle {
	private final String licensePlate;

	protected Vehicle(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

}
