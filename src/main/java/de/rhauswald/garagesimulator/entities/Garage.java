package de.rhauswald.garagesimulator.entities;

import java.util.List;
import java.util.Map;

public final class Garage {
	private final List<ParkingSpace> freeParkingSpaces;
	private final Map<String, ParkingSpace> usedParkingSpaces;

	public Garage(List<ParkingSpace> freeParkingSpaces, Map<String, ParkingSpace> usedParkingSpaces) {
		this.freeParkingSpaces = freeParkingSpaces;
		this.usedParkingSpaces = usedParkingSpaces;
	}

	public void park(Vehicle vehicle) throws NoFreeParkingLotsException {
		if (freeParkingSpaces.size() == 0) {
			throw new NoFreeParkingLotsException();
		}
		final ParkingSpace parkingSpace = freeParkingSpaces.remove(0);
		final String licensePlate = vehicle.getLicensePlate();
		usedParkingSpaces.put(licensePlate, parkingSpace);
	}

	public void unpark(Vehicle vehicle) {
		final ParkingSpace parkingSpace = usedParkingSpaces.remove(vehicle.getLicensePlate());
		if (parkingSpace == null) {
			throw new UnsupportedOperationException("Only vehicles parked in this garage can leave it. (Vehicle: " + vehicle.getLicensePlate() + ")");
		}
		freeParkingSpaces.add(parkingSpace);
	}

	public int freeParkingSpaces() {
		return freeParkingSpaces.size();
	}

	public ParkingSpace parkingSpaceOfVehicle(String licensePlate) {
		final ParkingSpace parkingSpace = usedParkingSpaces.get(licensePlate);
		return parkingSpace;
	}

	public int usedParkingSpaces() {
		return usedParkingSpaces.size();
	}

	public int getOverallNumberOfParkingSpaces() {
		final int freeParkingSpaces = freeParkingSpaces();
		final int usedParkingSpaces = this.usedParkingSpaces.size();
		final int overallNumberOfParkingSpaces = freeParkingSpaces + usedParkingSpaces;
		return overallNumberOfParkingSpaces;
	}
}
