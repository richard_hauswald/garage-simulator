package de.rhauswald.garagesimulator.entities;

public final class ParkingSpace {
	private final int level;
	private final int parkingSpaceNumber;

	public ParkingSpace(int level, int parkingSpaceNumber) {
		this.level = level;
		this.parkingSpaceNumber = parkingSpaceNumber;
	}

	public int getParkingSpaceNumber() {
		return parkingSpaceNumber;
	}

	public int getLevel() {
		return level;
	}

}
