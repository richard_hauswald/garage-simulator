package de.rhauswald.garagesimulator.entities.vehicles;

import de.rhauswald.garagesimulator.entities.Vehicle;

public final class Car extends Vehicle {
	public Car(String licensePlate) {
		super(licensePlate);
	}
}
