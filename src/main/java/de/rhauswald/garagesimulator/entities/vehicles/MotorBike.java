package de.rhauswald.garagesimulator.entities.vehicles;

import de.rhauswald.garagesimulator.entities.Vehicle;

public final class MotorBike extends Vehicle {
	public MotorBike(String licensePlate) {
		super(licensePlate);
	}
}
