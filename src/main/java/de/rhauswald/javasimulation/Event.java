/*
  File: Event.java

  Originally written by Keld Helsgaun and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.

  History:
  Date       Who                What
  5May2000   kh         Created public version
*/

package de.rhauswald.javasimulation;

/**
* This class may be used for event-based discrete event simulation.
* <p>
* Events are created as instances of <tt>Event</tt>-derived
* classes that override the abstract method <tt>actions</tt>.
* <p> 
* The <tt>actions</tt> method is used to specify the actions of
* of a class of events.
* 
*/
public abstract class Event {
    /**
    * The actions of this event.
    */
    protected abstract void actions();

    /**
    * Schedules this event to occur 
    * at the specified event time.
    * <p> 
    * The event is inserted into the event list at a
    * position corresponding to the current simulation time,
    * and <b>after</b> any processes with the same event time.
    *  
    * @param evTime The event time.
    *
    */	
    public final void schedule(final double evTime) {
        if (evTime < time)
            throw new RuntimeException("attempt to schedule event in the past");
        cancel();
        eventTime = evTime;
        Event ev = SQS.pred; 
        while (ev.eventTime > eventTime)
            ev = ev.pred;
        pred = ev; 
        suc = ev.suc;
        ev.suc = suc.pred = this;
    }

    /**
    * Cancels this scheduled event.
    * <p>
    * The event is removed from the event list.
    * If it is not scheduled, the call has no effect.
    */	
    public final void cancel() {
        if (suc != null) {
            suc.pred = pred;
            pred.suc = suc;
            suc = pred = null;
        }
    }

    /**
    * Returns the current simulation time.
    */	
    public static double time() {
        return time; 
    }

    /**
    * Runs a simulation for a specified period of simulated time.
    * <p> 
    * Time will start at <tt>0</tt> and jump from event time to event 
    * time until either this period is over, there are no 
    * more scheduled events, or the <tt>stopSimulation</tt> method 
    * is called.
    *  
    * @param period The length of the simulation period.
    */
    public static void runSimulation(double period) {
        time = 0;
        while (SQS.suc != SQS) {
            Event ev = SQS.suc;
            time = ev.eventTime;
            if (time > period)
                break;
            ev.cancel();
            ev.actions();
        }
        stopSimulation();
    }

    /**
    * Stops a simulation.
    */	
    public static void stopSimulation() {
        while (SQS.suc != SQS)
            SQS.suc.cancel();
        time = 0;
    } 

    /**
    * The head of the event list (a circular two-way list).
    */	
    private final static Event SQS;

   static {
        SQS  = new Event() {
            protected void actions() {}
        };
        SQS.pred = SQS.suc = SQS;
    }

    /**
    * The current simulation time
    */
    private static double time = 0;

    /**
    * The event time of this event (when scheduled).
    */	
    private double eventTime;

    /**
    * The predecessor of this event in the event list.
    */
    private Event pred;

    /**
    * The successor of this event in the event list.
    */
    private Event suc;
}
