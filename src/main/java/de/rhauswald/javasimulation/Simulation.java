/*
  File: Simulation.java

  Originally written by Keld Helsgaun and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.

  History:
  Date       Who                What
  5May2000   kh         Created public version
*/

package de.rhauswald.javasimulation;

/**
* This class is provided for user convenience.
* It may be used to extend the class
* that contains the main program of a simulation. 
*/

public class Simulation extends Event {
    protected final void actions() {}
} 
